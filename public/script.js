var scores = {};
var questions = [
    { id: 1, area: "Frontend", question: "Desenvolvimento Frontend (1 a 10):", next: 2 },
    { id: 2, area: "React", question: "React (1 a 10):", next: 3 },
    { id: 3, area: "Angular", question: "Angular (1 a 10):", next: 4 },
    { id: 4, area: "Vue", question: "Vue (1 a 10):", next: 5 },
    { id: 5, area: "JavaScript", question: "JavaScript (1 a 10):", next: 6 },
    { id: 6, area: "Node.js", question: "Node.js (1 a 10):", next: 7 },
    { id: 7, area: "TypeScript", question: "TypeScript (1 a 10):", next: 8 },
    { id: 8, area: "Backend", question: "Desenvolvimento Backend (1 a 10):", next: 9 },
    { id: 9, area: "Python", question: "Python (1 a 10):", next: 10 },
    { id: 10, area: "ASP.NET Core", question: "ASP.NET Core (1 a 10):", next: 11 },
    { id: 11, area: "Java", question: "Java (1 a 10):", next: 12 },
    { id: 12, area: "C++", question: "C++ (1 a 10):", next: 13 },
    { id: 13, area: "Spring Boot", question: "Spring Boot (1 a 10):", next: 14 },
    { id: 14, area: "Go Roadmap", question: "Go Roadmap (1 a 10):", next: 15 },
    { id: 15, area: "Rust", question: "Rust (1 a 10):", next: 16 },
    { id: 16, area: "GraphQL", question: "GraphQL (1 a 10):", next: 17 },
    { id: 17, area: "SQL", question: "SQL (1 a 10):", next: 18 },
    { id: 18, area: "DevOps", question: "DevOps (1 a 10):", next: 19 },
    { id: 19, area: "AWS", question: "AWS (1 a 10):", next: 20 },
    { id: 20, area: "Code Review", question: "Code Review (1 a 10):", next: 21 },
    { id: 21, area: "Docker", question: "Docker (1 a 10):", next: 22 },
    { id: 22, area: "Kubernetes", question: "Kubernetes (1 a 10):", next: 23 },
    { id: 23, area: "MongoDB", question: "MongoDB (1 a 10):", next: 24 },
    { id: 24, area: "Full Stack", question: "Desenvolvimento Full Stack (1 a 10):", next: 25 },
    { id: 25, area: "AI and Data Scientist", question: "Inteligência Artificial e Cientista de Dados (1 a 10):", next: 26 },
    { id: 26, area: "Game Developer", question: "Desenvolvimento de Jogos (1 a 10):", next: 27 },
    { id: 27, area: "Data Analyst", question: "Análise de Dados (1 a 10):", next: 28 },
    { id: 28, area: "Mobile Developer", question: "Desenvolvimento Aplicativos Ex. Android (1 a 10):", next: 29 },
    { id: 29, area: "React Native", question: "Desenvolvimento com React Native (1 a 10):", next: 30 },
    { id: 30, area: "Flutter", question: "Desenvolvimento com Flutter (1 a 10):", next: 31 },
    { id: 31, area: "Data Base", question: "Bancos de Dados. Ex. PostgreSQL (1 a 10):", next: 32 },
    { id: 32, area: "Blockchain", question: "Blockchain (1 a 10):", next: 33 },
    { id: 33, area: "QA", question: "Teste de Qualidade (1 a 10):", next: 34 },
    { id: 34, area: "Software Architect", question: "Arquitetura de Software (1 a 10):", next: 35 },
    { id: 35, area: "Cyber Security", question: "Segurança Cibernética (1 a 10):", next: 36 },
    { id: 36, area: "UX Design", question: "Design de Experiência do Usuário (1 a 10):", next: 37 },
    { id: 37, area: "MLOps", question: "Machine Learn Ops (1 a 10):", next: 38 },
    { id: 38, area: "Computer Science", question: "Computer Science (1 a 10):", next: 39 },
    { id: 39, area: "System Design", question: "System Design (1 a 10):", next: 40 },
    { id: 40, area: "Design and Architecture", question: "Design and Architecture (1 a 10):", next: 41 },
    { id: 41, area: "Design System", question: "Design System (1 a 10):", next: 42 },
    { id: 42, area: "Prompt Engineering", question: "Prompt Engineering (1 a 10):", next: 43 },
    { id: 43, area: "Data Structures & Algorithms", question: "Data Structures & Algorithms (1 a 10):", next: null }
];

function renderQuestion(questionId) {
    const question = questions.find(q => q.id === questionId);
    if (!question) {
        displayResults();
        return;
    }
    const container = document.getElementById('questionsContainer');
    container.innerHTML = `<div class="question" style="display:block;">
        <label>${question.question}</label>
        <div>` + Array.from({ length: 10 }, (_, i) => `<span class="interest-level" onclick="selectInterest(${i + 1}, '${question.area}', ${question.next})">${i + 1}</span>`).join('') + `</div></div>`;
}

function selectInterest(level, area, nextQuestionId) {
    scores[area] = level;
    if (nextQuestionId) renderQuestion(nextQuestionId);
    else displayResults();
}

function displayResults() {
    const name = document.getElementById('nameInput').value;
    const container = document.getElementById('questionsContainer');
    container.style.display = 'none'; // Hide questions
    document.getElementById('resultsChart').style.display = 'block'; // Show the chart
    const labels = Object.keys(scores);
    const data = {
        labels: labels,
        datasets: [{
            label: `${name}'s Aptidão`,
            backgroundColor: '#005c99',
            borderColor: '#005c99',
            data: Object.values(scores),
        }]
    };
    const config = { type: 'bar', data: data, options: { indexAxis: 'y', scales: { x: { beginAtZero: true } } } };
    new Chart(document.getElementById('resultsChart'), config);

    // Determine the top 5 areas based on scores
    const sortedAreas = Object.keys(scores).sort((a, b) => scores[b] - scores[a]).slice(0, 5);
    const aptitudeInfo = document.getElementById('aptitudeInfo');
    aptitudeInfo.innerHTML = `
        <p>Suas cinco maiores áreas de aptidão são:</p>
        <table style="margin: 0 auto; text-align: center;">
            <tr>
                <th style="padding: 10px;">Área</th>
                <th style="padding: 10px;">Aptidão</th>
            </tr>
            ${sortedAreas.map(area => `
                <tr>
                    <td style="padding: 10px;">${area}</td>
                    <td style="padding: 10px;">${scores[area]}</td>
                </tr>`).join('')}
        </table>
        <p style="text-align: center;">Priorize <a href="choices.html">aqui</a> ou para saber mais clique <a href="https://roadmap.sh/">aqui</a>.</p>`;
    aptitudeInfo.style.display = 'block'; // Show aptitude info
}



function startTest() {
    const nameInput = document.getElementById('nameInput').value;
    if (nameInput.trim() !== '') {
        document.getElementById('nameContainer').style.display = 'none';
        document.getElementById('questionsContainer').style.display = 'block';
        renderQuestion(1); // Render first question on load
    } else alert('Por favor, insira seu nome.');
}
